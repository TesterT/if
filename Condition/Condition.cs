using System;

namespace Condition
{
    public static class Condition
    {
        /// <summary>
        /// Implement code according to description of  task 1
        /// </summary>        
        public static int Task1(int n)
       {
            int result;
            if (n == 0)
            {
                result = 0;
                
                //Retest 
                //result = Int32.MaxValue;
                //result = 1;
                //result = -1;
                return result;
            }
            else if (n > 0)
            {
                result = Convert.ToInt32(Math.Pow(n, 2));
                return result;
            }
            else if (n < 0)
            {
                result = Math.Abs(n);
                //result = n; 
                //result = 0;
                return result;
            }
            else return Int32.MaxValue;
            
        }
        
        public static int Task2(int n)
        {
            int result = 0;
            if (n >= 100 && n <=999)
            {
                int first = n - n / 10 * 10;
                int second = n / 10;
                second = second - second / 10 * 10;
                int third = n / 100;
                if (first == second && first == third)
                {
                    return n;
                }
                if (first > second && first > third)
                {
                    result = first * 100;
                    if (second > third)
                    {
                        result = result + second * 10 + third;
                    } 
                    else
                    {
                        result = result + third * 10 + second;
                    }
                }
                if (second > first && second > third)
                {
                    result = second * 100;
                    if (first >= third)
                    {
                        result = result + first * 10 + third;
                    }
                    else
                    {
                        result = result + third * 10 + first;
                    }
                }
                if (third > first && third > second)
                {
                    result = third * 100;
                    if (first > second)
                    {
                        result = result + first * 10 + second;
                    }
                    else
                    {
                        result = result + second * 10 + first;
                    }
                }
                if (first == second & first > third)
                {
                    result = first * 100 + second * 10 + third;
                }
                else if (second == third & second > first) 
                {
                    result = second * 100 + third * 10 + first;
                } else if (first == third & first > second)
                {
                    result = first * 100 + third * 10 + second;
                }
            }
            else if (n < 100)
            {
                Console.WriteLine("The number is less than 100  and result is 0");
            }
            else
            {
                Console.WriteLine("The number is greater than 999 and result is 0");
            }
            return result;
            
        }
    }
}